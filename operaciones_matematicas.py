#Manejo de excepciones (try - except)
"""
try:
    numero_1 = "5"
    numero_2: int = 10
    mensaje: str = "c"
    #Casteo de variable (convierte un tipo de dato a otro)
    numero_1 = int(numero_1)
    suma = numero_1 + numero_2
    resp = "La suma es: "+str(suma)
    print( resp )
    print( numero_1 )
    #Evalúa si el número es par o impar (% es módulo)
    if numero_1 % 2 == 0:
        print("Par")
    else:
        print("Impar")
except:
    print("Error")
"""

"""
Input (entrada de datos)
"""
"""
try:
    var = int(input("Por favor ingrese un número:"))
except:
    print("Debe de ingresar un número")
"""
"""
---------------------------
--Recomendación musical----
---------------------------
¿Qué género musical te gusta ?
Ingrese 1 para un sí o 0 para un No.
Reggaeton:_
Salsa:_
Bachata:_

Mostrar una canción correspondiente al género musical
que le guste al usuario
"""
"""
#Juan manuel
try:
    genero_musical = input("¿Que genero musical te gusta?, ingrese 1 para un si y 0 para un no")
    reggaeton = int(input("Reggaeton: "))
    salsa = int(input("Salsa: "))
    bachata = int(input("Bachata: "))
    dustep = int(input("Dustep: "))

    if reggaeton == 1:
        print("Te recomiendo la siguiente cancion de reggaeton Ozuna x karol g Myke towers https://www.youtube.com/watch?v=KilybZma5vY")
    if salsa == 1: 
        print("Te recomiendo la siguiente cancion de salsa de Yiyo sarante - corazon de acero https://www.youtube.com/watch?v=RUV55lGPGaY") 
    if bachata == 1: 
        print("Te recomiendo la siguiente cancion de bachata Romeo santos -propuesta indecente https://www.youtube.com/watch?v=QFs3PIZb3js")
    if dustep == 1:
        print("Te recomiendo la siguiente cancion de dustep de gutter-grave https://www.youtube.com/watch?v=zvquZnQAoO4")
except:
    print("error")
"""
#Mauricio
from os import system
print ("Géneros Musicales")
system("clear")

print("Por favor para cada género ingrese 1 si es de su gusto o 0 en caso contrario")
try:
    aux1 = int(input("¿Le gusta el Reggaeton? "))
    aux2 = int(input("¿Le gusta La salsa? "))
    aux3 = int(input("¿Le gusta la Bachata? "))
except:
    print("Ha seleccionado una opción inválida, hasta pronto")

if aux1 == 1:
    print("Cancion recomenda: vuelve (Don Omar)")
if aux2 == 1:
        print("Cancion recomenda: nunca te dire adios (Max Torres)")
if aux3 == 1:
        print("Cancion recomenda: El beso (Romeo)")
if aux1 == aux2 == aux3:
    print("Sus géneros preferidos no se encuentran en la lista")





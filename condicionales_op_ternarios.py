
from os import system

system("clear")

# Operaciones ternarias
"""
var1 = 5
var2 = 8
resultado = 'Cumple' if var1 > var2 else 'No cumple'
print(resultado)
"""
"""
j = "8.9"
#Castear (convertir) un tipo de dato a otro
#Convertimos un string a un flotante
n = float(j)
#Convertir un entero a un string
entero = 10
cadena = str(entero)
"""
"""
nuevo_tipo_dato = tipo_dato(lo_que_quiero_convertir)
"""

"""
El input retorna un string
por eso es importante
"""
"""
num1 = int( input("Por favor ingrese un número: ") )
num2 = int( input("Por favor ingrese otro número: ") )
suma = num1 + num2
print("La suma es: ")
print(suma)
#Terminar el programa
exit(0)
"""
"""
import random
longitud:int = 3

abecedario:str = '51630A'
# Elije aleatoriamente 5 elementos del abecedario
desordenar = random.sample(abecedario, longitud)
# join crea cadenas a partir de objetos iterables 
palabra:str = ''.join(desordenar)

print(palabra)
"""

num = 10
num2 = 5
# Condición padre o principal
if num == 10:
    # Condición hijo o condición anidada
    if num2 == 5:
        print("Condición anidada")
    elif num2 < 5:
        print("Fuera del if")
else:
    # Condición anidada
    if num > 10:
        # Condición anidada
        if num == 12:
            print("")

# Muestro información
#print("Muestra informacion")
# Solicito información
#info = input("Por favor digite una tecla: ")
# Muestro la información ingresada por el usuario
#print("El usuario ingresó:", info)
"""
var = "5"
#Casteamos (convertimos) un tipo de dato a otro
numero1 = var#int(var)
numero2 = 5
#Operación matematica
suma = numero1 + numero2
print(suma)
"""
"""
#Pedimos un numero por consola
numero1 = float(input("Por favor ingrese un número: "))
#Pedimos un numero por consola
numero2 = float(input("Por favor ingrese otro número: "))
#Operamos las variables
suma = + numero2
print(suma)
"""
"""
#Manejo de excepciones
try:
    numero1 = float(input("Por favor ingrese un número: "))
    # Pedimos un numero por consola
    numero2 = float(input("Por favor ingrese otro número: "))
    # Operamos las variables
    suma = numero1 + numero2
    print(suma)
except:
    print("Debe digitar un número")
    #Terminar el programa
    exit(0)
"""
"""
Desarrollar una función que se implementará en un cajero,
esta función será la encargada de procesar los
retiros de dinero.
Reciba como parámetros la cantidad a retirar y
el saldo inicial del usuario.
Si la cantidad a retirar es SUPERIOR a $20.000
el cajero cobrará una comisión de $2.000.
Si el monto supera el saldo inicial muestre en pantalla
"El valor a retirar supera el saldo"
En caso de realizar el retiro de forma exitosa,
mostrar el saldo que al cliente le queda.
"""

"""
#Kevin
print("Bienvenido al cajero automatico")

try:
    saldo=int(input("Por favor digite su saldo actual"))
    while saldo < 0:
        print("Por favor digite un valor superior a 50 pesos")
        saldo = input()
except ValueError:
    print ("Se ha digitado un valor no numero, reinicie el sistema")
    exit()

try:
    retiro=int(input("Digite la cantidad a retirar"))
except ValueError:
    print ("Se ha digitado un valor no numero o numero negativo, reinicie el sistema")
    exit()

while retiro > saldo:
    print("usted no puede retirar esa cantidad recuerde que su saldo es de ", saldo)
    x = int(print("Seleccione 1 para elegir otro valor u otro numero para finalizar el sistema"))
    if x==1:
        while retiro > saldo:
            retiro = int(print("Ingrese un retiro valido. SALDO: ", saldo))
    else:
        print("Hasta la proxima")
        exit()
    retiro=int(print("¿cuanto desea retirar?"))

if retiro > 20000:
    saldores=saldo-retiro-20000
    print("El cajero le dara su dinero. recuerdo que su saldo restante es de ", saldores)
else:
    saldores=saldo-retiro
    print("El cajero le dara su dinero. recuerdo que su saldo restante es de ", saldores)

print("Feliz dia")
"""




#Miguel
"""
def cajero (saldo:float,retiro:float):
    if retiro > 20000:
        retiro = retiro + 2000
    if retiro > saldo:
        respuesta="El valor a retirar es superior al saldo"
    else:
        saldo-=retiro
        respuesta=saldo
    return respuesta

try:
    saldo=float(input("Ingrese su saldo inicial: $"))
    retiro=float(input("Ingrese cuanto desea retirar: $"))
    print(cajero(saldo,retiro))
except:
    print("Ingrese datos numericos")
"""
#Ovidio
"""
try:
    saldo = int(input('Saldo de cuenta: '))
    retiro = int(input( 'Cantidad a retirar: '))
except:
        print('Solo se permite ingresar numeros!')
        exit(0)
nuevo_saldo: int = 0

def validacion_retiro(saldo, retiro):
        if retiro <= 20000:
            nuevo_saldo = saldo - retiro
            if nuevo_saldo >= 0:
                return nuevo_saldo
            else:
                print("El valor a retirar supera el saldo")
                exit(0)
        else:
            comision: int = 2000
            nuevo_saldo = saldo - retiro - comision
            if nuevo_saldo >= 0:
                return nuevo_saldo
            else:
                print("El valor a retirar supera el saldo")
                exit(0)
    
            
x = validacion_retiro(saldo, retiro)
            



print('Su nuevo saldo es: ', x)
"""
"""
#EMOJIS
print("\U0001f600")
print("\U0001F606")
print("\U0001F923")
print("\U0001F618")
print("\U0001F917")
print("Solo se aceptan números \U0001F62A")
"""


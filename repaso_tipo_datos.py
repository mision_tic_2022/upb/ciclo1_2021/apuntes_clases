
#Crea variable de tipo entero
numero_entero: int = 10
#Flotante
numero_flotante:float = 5.5
#String
variable_string: str = "Hola mundo"

#booleanas
#1-> verdad
#0 -> falso
p: bool = 0
q: bool = False
z: bool = 1

#Condicional simple
if 5<=3 and 10 == 20:
    print("esto es verdad")
    print("Dentro del primer if")
else:
    print("mentira")
#Condicional secuencial, cuando tengo varios if después del otro
if 5==5:
    print("son iguales")

#Condicionales en cascada
if p and q:
    print("Se cumple la condición")
elif p == True:
    print("Unicamente P es verdadero")
elif q == True:
    print("únicamente Q es verdadero")
else:
    print("Ninguno es verdadero")

print("FIn de los condicionales")
